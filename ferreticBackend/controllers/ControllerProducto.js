var mongoose = require('../src/conexBD/conn');
var Producto = require('../models/producto');

function prueba(req, res) {
    res.status(200).send({
        message: 'probando una acción'
    });
}

function saveProducto(req, res) {
    var myProducto = new Producto(req.body);
    myProducto.save((err, result) => {
        res.status(200).send({ message: result });
    });
}

function buscarData(req, res) {
    var idProducto = req.params.id;
    Producto.findById(idProducto).exec((err, result) => {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' });
            } else {
                res.status(200).send({ result });
            }
        }
    });
}

function listarAllData(req, res) {
    var idProducto = req.params.id;
    if (!idProducto) {
        var result = Producto.find({}).sort('codigo_barra');
    } else {
        var result = Producto.find({ _id: idProducto }).sort('codigo_barra');
    }
    result.exec(function (err, result) {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' });
            } else {
                res.status(200).send({ result });
            }
        }
    })
}

function updateProducto(req, res) {
    var id = mongoose.Types.ObjectId(req.params.id);
    Producto.findOneAndUpdate({_id: id}, req.body, { new: true }, function (err, Producto) {
        if (err) {
            return res.json(500, {
                message: "No existe producto"
            })
        } else {
            res.json(Producto);
        }
    });
}

function deleteProducto(req, res) {
    var idProdcuto = req.params.id;
    Producto.findByIdAndRemove(idProdcuto, function (err, producto) {
        if (err) {
            returnres.json(500, {
                message: 'No hemos encontrado la carrera'
            })
        }
        return res.json(producto)
    });
}

module.exports = {
    prueba,
    saveProducto,
    buscarData,
    listarAllData,
    updateProducto,
    deleteProducto
}