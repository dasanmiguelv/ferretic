const { Router } = require('express');
var ControllerProducto = require('../controllers/ControllerProducto');
const router = Router();
//export default router;


router.get('/prueba', ControllerProducto.prueba);
router.post('/crearProducto', ControllerProducto.saveProducto);
router.get('/buscarProducto/:id', ControllerProducto.buscarData);
router.get('/buscarall/:id?', ControllerProducto.listarAllData);
router.delete('/borrarProducto/:id', ControllerProducto.deleteProducto);
router.put('/actualizarProducto/:id', ControllerProducto.updateProducto);
module.exports = router;