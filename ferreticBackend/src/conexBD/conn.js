/*const mongoose = require('mongoose');

//mongoose.connect(process.env.DATABASE_URI, {
mongoose.connect("mongodb://localhost:27017/ferreticBD", {
    useNewUrlParser: true,
    //useCreateIndex: true,
    useUnifiedTopology: true,
    //useFindAndModify: false,
}, (err, res) => {
    if (err) {
        throw err;
    } else {
        console.log('La conexion a la base de datos fue correcta...')
    }
});

module.exports = mongoose;*/

const mongoose = require('mongoose'); 

mongoose.connect(process.env.DATABASE_URI, function(err, res) {
	if(err) {
		console.log('ERROR: connecting to Database. ' + err);
	} else {
		console.log('Connected to Database');
	}
});

module.exports = mongoose;