// importamos las dependencias para el manejo del proyecto

var express = require("express");
var app = express();
var bodyParser = require("body-parser");
//methodOverride = require("method-override");
mongoose = require('mongoose');
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

// Configurar cabeceras y cors para los permisos sobre el backend
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.use(require('./routers/router'));

//Permite que el archivo app.js sea global y se use desde cualquier lugar
module.exports = app;