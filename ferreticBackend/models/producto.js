var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductoSchema = Schema({
    codigo_barras: String,
    nombre: String,
    descripcion: String,
    categoria: String,
    id_persona: Number,
    precio: Number,
    stock_minimo: Number
});

const Producto = mongoose.model('producto', ProductoSchema);
module.exports = Producto;