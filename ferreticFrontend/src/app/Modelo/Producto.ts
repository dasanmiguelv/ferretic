export class Producto{
    _id:String="";
    codigo_barras:String = "";
    nombre:String = "";
    descripcion:String ="";
    categoria:String="";
    id_persona:number=0;
    precio:number=0;
    stock_minimo:number=0;
}