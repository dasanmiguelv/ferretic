import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Producto } from 'src/app/Modelo/Producto';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }
  Url = 'http://localhost:4000/';
      

  getProductos() {
    return this.http.get<Producto[]>(this.Url + "buscarall");
  }

  createProducto(producto: Producto) {
    return this.http.post<Producto>(this.Url, producto);
  }

  getProducto(_id: number) {
    return this.http.get<Producto>(this.Url + "/actualizarProducto" + _id);
  }

  updateProducto(producto: Producto) {
    return this.http.put<Producto>(this.Url + "/" + producto._id, producto);
  }

  deleteProducto(producto: Producto) {
    return this.http.delete<Producto>(this.Url + "/" + producto._id);
  }
}

