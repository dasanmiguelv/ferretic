import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from 'src/app/Modelo/Producto';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  productos: Producto[] = [];
  constructor(private router: Router, private service: ServiceService) { }

  ngOnInit(): void {  
    this.service.getProductos()
      .subscribe(data => {
        this.productos = data;
      })
  }

  Edit(producto:Producto):void{
    localStorage.setItem("id", producto._id.toString());
    this.router.navigate(["edit"]);
  }

  Delete(producto:Producto){
    this.service.deleteProducto(producto)
    .subscribe(data=>{
      this.productos=this.productos.filter(p=>p!==producto);
      alert("Producto borrado con exito.. !!");
    })
  }
}
