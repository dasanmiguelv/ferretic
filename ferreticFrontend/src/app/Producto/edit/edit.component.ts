import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from 'src/app/Modelo/Producto';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  producto: Producto = new Producto();
  constructor(private router: Router, private service: ServiceService) { }

  ngOnInit(): void {
    //this.Edit();
  }
  /*Edit() {
    let _id = localStorage.getItem("_id");
    this.service.getProducto(+_id)
      .subscribe(data => {
        this.producto = data;
      })
  }*/

  Actualizar(producto: Producto) {
    this.service.updateProducto(producto)
      .subscribe(data => {
        alert("Se actualizó con exito !!")
        this.router.navigate(['list']);
      })
  }

}
